package com.epam.factorymethod;

import org.openqa.selenium.WebDriver;

public interface WebDriverFactory {
	WebDriver create();

}
