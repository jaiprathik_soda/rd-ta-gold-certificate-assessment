package com.epam.factorymethod;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

public class MyFirefoxDriver implements WebDriverFactory{

	@Override
	public WebDriver create() {
        return new FirefoxDriver();
	}

}
