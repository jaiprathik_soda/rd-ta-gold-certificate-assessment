package com.epam.factorymethod;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.edge.EdgeDriver;

public class MyEdgeDriver implements WebDriverFactory{
	@Override
    public WebDriver create() {
        return new EdgeDriver();
    }
}

