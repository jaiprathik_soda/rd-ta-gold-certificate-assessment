package com.epam.factorymethod;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class MyChromeDriver implements WebDriverFactory {
	@Override
	public WebDriver create() {
        return new ChromeDriver();
	}
	

}
