package com.epam.utilities;

import java.io.FileInputStream;
import java.util.Properties;

import com.epam.exception.CustomException;

public class FileReader {
	 private FileReader()
	    {

	    }
	    public static Properties readFiles(String fileName) throws CustomException
	    {
	        Properties properties=null;
	        try (FileInputStream fileInputStream=new FileInputStream(fileName)){
	            properties=new Properties();
	            properties.load(fileInputStream);
	        }
	        catch (Exception e)
	        {
	            throw new CustomException("Recheck the property file path");
	        }
	        return properties;
	    }
		
}
