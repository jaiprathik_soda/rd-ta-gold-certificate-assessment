package com.epam.utilities;

import org.testng.ITestListener;
import org.testng.ITestResult;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;


	public class Listener extends Screenshots implements ITestListener{
		
		private static final Logger Logger  = LogManager.getLogger(Listener.class);
		
		  @Override
		  public void onTestStart(ITestResult result) {
			 Logger.info(result.getName()+" test case started");	
		  }
		  public void onTestSuccess(ITestResult result) {
			  Logger.info("The name of the testcase successed is :"+result.getName());
		  }
		  public void onTestFailure(ITestResult result) {
			  if(result.getStatus()==ITestResult.FAILURE) {
				  try {
					 saveScreenshot();
				  } catch (Exception expection) {
		           Logger.error("Failed to save screenshot: " + expection.getLocalizedMessage());
		        }
			  }
		  }
		  public void onTestSkipped(ITestResult result) {
			  Logger.info("The name of the testcase Skipped is :"+result.getName());
		  }
}
	

