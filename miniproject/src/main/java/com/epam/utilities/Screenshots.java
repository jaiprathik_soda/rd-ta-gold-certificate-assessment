package com.epam.utilities;

import java.io.File;
import java.io.IOException;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import org.apache.commons.io.FileUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;

import com.epam.drivers.DriverSingleton;
public class Screenshots {
	 private static final Logger LOGGER = LogManager.getRootLogger();
	 public void saveScreenshot()  {
		 File screenCapture = ((TakesScreenshot) DriverSingleton
	                .getDriver())
	                .getScreenshotAs(OutputType.FILE);
	        try {
	            FileUtils.copyFile(screenCapture, new File(
	                    ".//target/screenshots/"
	                            + getCurrentTimeAsString() +
	                            ".png"));
	        } catch (IOException exception) {
	            LOGGER.error("Failed to save screenshot: " + exception.getLocalizedMessage());
	        }
	    }
	 
	    public String getCurrentTimeAsString(){
	        DateTimeFormatter formatter = DateTimeFormatter.ofPattern( "uuuu-MM-dd_HH-mm-ss" );
	        return ZonedDateTime.now().format(formatter);
	    }
}