package com.epam.drivers;

import com.epam.pages.BasePage;
import io.cucumber.java.After;
import io.cucumber.java.Before;

public class Hooks extends BasePage {

    @Before("@uitesting")
    public static void setUp(){
        try {
            driver=DriverSingleton.getDriver();
            driver.manage().window().maximize();
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }
    @After("uitesting")
    public static void tearDown()
    {
        driver.close();
    }
}
