package com.epam.drivers;

import org.picocontainer.DefaultPicoContainer;
import org.picocontainer.MutablePicoContainer;

import com.epam.pages.HomePage;
import com.epam.pages.LoginPage;

public class PicoContainer {
	private MutablePicoContainer container;
	public PicoContainer() {
		container = new DefaultPicoContainer();
		container.addComponent(HomePage.class);
		container.addComponent(LoginPage.class);
	}
	public MutablePicoContainer getPicoContainer() {
		return container;
	}
}