package com.epam.drivers;

import org.openqa.selenium.WebDriver;
import static com.epam.constants.FileConstants.BROWSER_PATH;
import com.epam.exception.CustomException;
import com.epam.factorymethod.MyChromeDriver;
import com.epam.factorymethod.MyEdgeDriver;
import com.epam.factorymethod.MyFirefoxDriver;
import com.epam.factorymethod.WebDriverFactory;
import com.epam.utilities.FileReader;

public class DriverSingleton {
	private static WebDriver driver;
    private DriverSingleton()
    {

    }
    public static synchronized void setDriver() throws CustomException {
        switch (FileReader.readFiles(BROWSER_PATH).getProperty("BROWSER"))
        {
            case "CHROME":
            {
            	WebDriverFactory browser=new MyChromeDriver();
                driver=browser.create();
                break;
            }
            case "EDGE":
            {
            	WebDriverFactory browser=new MyEdgeDriver();
                driver=browser.create();
                break;
            }
            case "FIREFOX":
            {
            	WebDriverFactory browser=new MyFirefoxDriver();
                driver=browser.create();
                break;
            }
            default:
                throw new CustomException("launch Correct broswer");
        }
    }
    public static synchronized  WebDriver getDriver()
    {
        try {
        setDriver();
    }
    catch (Exception e)
    {
        e.printStackTrace();
    }
        return driver;
    }
	

}
