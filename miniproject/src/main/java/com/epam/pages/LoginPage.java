package com.epam.pages;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class LoginPage extends BasePage{
	public LoginPage() {
		PageFactory.initElements(BasePage.driver, this);
	}
	@FindBy(xpath = "//input[@id='userName']")
	private WebElement userName;
	@FindBy(xpath = "//input[@id='password']")
	private WebElement userPassword;
	@FindBy(xpath = "//button[@id='login']")
	private WebElement loginButton;
	JavascriptExecutor js = (JavascriptExecutor)driver;	
	public void enterLoginDetails(String name, String password) {
		userName.sendKeys(name);
		userPassword.sendKeys(password);
	}
	public void login()  {
		js.executeScript("arguments[0].click();", loginButton);
	}
	

}
