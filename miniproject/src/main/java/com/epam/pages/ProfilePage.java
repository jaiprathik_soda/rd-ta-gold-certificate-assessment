package com.epam.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class ProfilePage {
	public ProfilePage() {
		PageFactory.initElements(BasePage.driver, this);
	}
	@FindBy(xpath = "//button[@id='gotoStore']")
	private WebElement redirectionToBookStoreElement;
	public void redirectToBooksPage() {
		redirectionToBookStoreElement.click();
	}
	

}
