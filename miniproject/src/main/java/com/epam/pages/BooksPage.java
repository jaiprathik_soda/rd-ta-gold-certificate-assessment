package com.epam.pages;

import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class BooksPage {
	public BooksPage() {
		PageFactory.initElements(BasePage.driver, this);
	}
	@FindBy(xpath = "//*[@id=\"app\"]/div/div/div[2]/div[2]/div[2]/div[2]/div[1]/div[1]/div/div[2]/div[1]")
	private List<WebElement> title;
	@FindBy(xpath = "//*[@id=\"app\"]/div/div/div[2]/div[2]/div[2]/div[2]/div[1]/div[1]/div/div[3]/div[1]]")
	private List<WebElement> author;
	@FindBy(xpath = "//*[@id=\"app\"]/div/div/div[2]/div[2]/div[2]/div[2]/div[1]/div[1]/div/div[4]/div[1]")
	private List<WebElement> publisher;
	public List<WebElement> getTitile() {
		return title;
		
	}
	public List<WebElement> getAuthor() {
		return author;
		
	}
	public List<WebElement> getPublisher() {
		return publisher;
		
	}
	 
   

}
