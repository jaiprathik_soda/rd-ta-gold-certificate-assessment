package com.epam.pages;
import static com.epam.constants.UIConstants.WEBSITE_URL;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class HomePage extends BasePage {
	public HomePage() {
		PageFactory.initElements(BasePage.driver, this);
	}
	@FindBy(xpath = "//button[@id='login']")
	private WebElement loginElement;
	@FindBy(xpath = "//label[@id='userName-value']")
	private WebElement userNameElement;
	@FindBy(xpath="//button[@id='submit']")
	private WebElement logOutButton;
	public void getHomePage()
	{
		driver.get(WEBSITE_URL);
	}
	public void clickLoginButton() {
		loginElement.click();
	}
	
	public String getUserName() {
		return userNameElement.getText();
	}
	public void logOut() {
		logOutButton.click();
	}
	
}
