package com.epam.runner;

import org.testng.annotations.Listeners;

import io.cucumber.testng.AbstractTestNGCucumberTests;
import io.cucumber.testng.CucumberOptions;

@CucumberOptions(plugin = {"com.aventstack.extentreports.cucumber.adapter.ExtentCucumberAdapter:"},
glue = {"com.epam"},
features={"src/test/resources/features"})
@Listeners({com.epam.utilities.Listener.class})
public class BaseRunner extends AbstractTestNGCucumberTests
	{
	
	}