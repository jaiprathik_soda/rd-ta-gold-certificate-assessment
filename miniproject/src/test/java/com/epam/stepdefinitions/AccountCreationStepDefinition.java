package com.epam.stepdefinitions;
import static com.epam.constants.APIConstants.*;
import static io.restassured.RestAssured.*;

import org.json.JSONObject;
import org.testng.Assert;

import com.epam.pojos.UserCreatedPojo;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.restassured.RestAssured;
import io.restassured.response.Response;

public class AccountCreationStepDefinition {
	
	 public Response response;
	 @Given("user loads the URI")
	 public void user_loads_the_uri() {
		 RestAssured.baseURI=BASE_URI;
		 RestAssured.basePath="/Account/v1/User";
	 }
	 @When("user sends post request with details to create account")
	 public void user_sends_post_request_with_details_to_create_account() {
		 JSONObject requestParams = new JSONObject();
	        requestParams.put("userName", USER_NAME);
	        requestParams.put("password", PASSWORD); 
	        response = given()
	            .header("Content-Type", "application/json")
	            .body(requestParams.toString())
	            .when()
	            .post();
	        response.prettyPrint(); 
	 }

	 @Then("the response code {int} is displayed")
	 public void the_response_code_is_displayed(Integer responseCode) {
		 Assert.assertEquals(responseCode,response.getStatusCode());
	 }

	 @Then("the response body should contain same username")
	 public void the_response_body_should_contain_same_username() {
		 UserCreatedPojo pojo = response.as(UserCreatedPojo.class);
			Assert.assertTrue(pojo.getUsername().contains(USER_NAME));
	 }
		
			
}
