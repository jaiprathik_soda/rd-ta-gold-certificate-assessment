package com.epam.stepdefinitions;

import java.util.Map;

import org.testng.Assert;

import com.epam.drivers.PicoContainer;
import com.epam.pages.HomePage;
import com.epam.pages.LoginPage;
import io.cucumber.datatable.DataTable;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

public class UserLoginStepDefinition {
	
	 private HomePage homePage;
	 private LoginPage loginPage;
	 public UserLoginStepDefinition(PicoContainer picoContainer)
	    {
	        homePage=picoContainer.getPicoContainer().getComponent(HomePage.class);
	        loginPage=picoContainer.getPicoContainer().getComponent(LoginPage.class);
	    }
	@Given("user enters the webpage URL")
	public void user_enters_the_webpage_url() {
	    homePage.getHomePage();
	}

	@When("user selects LogIn Page")
	public void user_selects_log_in_page() {
	    homePage.clickLoginButton();
	}

	@When("user enters the LogIn details:")
	public void user_enters_the_log_in_details(DataTable dataTable) {
		Map<String, String> data = dataTable.asMap(String.class, String.class);
		String userName = data.get("UserName");
		String userPassword = data.get("Password");
		loginPage.enterLoginDetails(userName, userPassword);
	}

	@When("user selects LogIn")
	public void user_selects_log_in() {
		loginPage.login();
	}
	@Then("user validates Username as {string} on the Books Dashboard page")
	public void user_validates_username_as_on_the_books_dashboard_page(String name) {
		Assert.assertEquals(homePage.getUserName(),name);
	}
}
