package com.epam.stepdefinitions;
import static com.epam.constants.APIConstants.*;

import static io.restassured.RestAssured.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.openqa.selenium.WebElement;
import org.testng.Assert;

import com.epam.drivers.PicoContainer;
import com.epam.pages.BooksPage;
import com.epam.pages.HomePage;
import com.epam.pojos.BookPojo;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.restassured.RestAssured;
import io.restassured.response.Response;

public class BookStepDefinition {
	private HomePage homePage;
	private BooksPage booksPage;
	 public BookStepDefinition(PicoContainer picoContainer)
	    {
	        homePage=picoContainer.getPicoContainer().getComponent(HomePage.class);
	        booksPage=picoContainer.getPicoContainer().getComponent(BooksPage.class);
	    }
	Response bookResponse;
	@Given("user loads the Bookstore URI")
	public void user_loads_the_bookstore_uri() {
	    RestAssured.baseURI=BASE_URI;
	    RestAssured.basePath="/BookStore/v1/Books";
	}
	@When("user sends get request to retrieve book details")
	public void user_sends_get_request_to_retrieve_book_details() {
		 bookResponse =  given()
                .when()
                .get();
		 bookResponse.prettyPrint();
	    	
	}
	@Then("validates if it has book {string}")
	public void validates_if_it_has_book(String inputTitle) {
			BookPojo bookPojo = bookResponse.as(BookPojo.class);
			Assert.assertEquals(bookPojo.getBooks().getTitle(),inputTitle);	
	}
	@Given("user enters the book URL")
	public void user_enters_the_book_url() {
	    homePage.getHomePage();
	}

	@Then("validate the captured details")
	public void validate_the_captured_details() {
		String responseBody = bookResponse.getBody().asString();
		Map<String, List<WebElement>> myMap = new HashMap<>();
		myMap.put(responseBody, booksPage.getTitile());
		List<WebElement> list = myMap.get(responseBody);
        for (WebElement uiElement : list) {
            Assert.assertEquals(responseBody,uiElement);
        }
  
	}
	

}
