Feature: Validating the Book Details.

  Scenario: User attempts to retrieve book details through API
    Given user loads the Bookstore URI 
    When user sends get request to retrieve book details
    Then validates if it has book "Git Pocket Guide"
    
 Scenario: User validating the captured details of books
    Given user enters the book URL 
   	Then validate the captured details