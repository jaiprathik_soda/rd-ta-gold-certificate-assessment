Feature: Checking User Account Creation Functionality

  Scenario: User attempts to create account through API
    Given user loads the URI
    When user sends post request with details to create account
    Then the response code 201 is displayed 
    And the response body should contain same username
